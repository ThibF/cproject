#ifndef CPROJECT_STORAGE_H
#define CPROJECT_STORAGE_H

#include "defs.h"
#include "storage_bsp.h"
#include "storage_crc.h"
/*! \mainpage CProject
 *
 * \section intro_sec Introduction
 *
 * This is a little cproject with unit-testing (ceedling) and documentation(documentation). They are both launched by gitlab followed by the documentation deployment through Gitlab Pages.
 *
 * the main interface is <a href="storage_8h.html">here</a>
 *
 * <a href="https://gitlab.com/ThibF/cproject">Gitlab project</a>
 *
 * \section sw_sec SW Assessment
 *
 * Exercise
 * The exercise exists in the implementation of a non-volatile memory storage
 * component. This component should be able to backup and restore values
 * corresponding to a unique attribute identifier. For the simplicity of the exercise the
 * underlying non-volatile memory (eeprom, flash, ...) can be modelled as a file.
 *
 * \section impro_sec Possible tooling improvement
 * Add testing to storage_bsp.c
 *
 * Generate test coverture.
 *
 * Put this section in a seperate file.
 *
 * Add a README.md.
 *
 * Add a Licence.
 */


/*! \file storage.h
    \brief Interface for storage into non-volatile memory.
    Can store up to 255 bytes with crc checked at reading time.
    It doesn't check for crc at writing time.
    Warning : It doesn't use malloc, you need to allocate the data array up to 255 before call gpNvm_Result.
*/

/*! \fn gpNvm_Result gpNvm_GetAttribute(gpNvm_AttrId attrId, UInt8* pLength, UInt8* pValue);
    \brief Retrieve data associated to targeted \a attrId and fill \a pLength with data length retrieved, \a pValue array will contain data, it should be already allocated.
    \param attrId id targeted to get data (input).
    \param pLength number of bytes read (output).
    \param pValue bytes array read, need to be allocated before, up to 256 bytes(output).
    \return success code, will take STORAGE_NVM_RETURN_CODE_OK value if success, STORAGE_NVM_RETURN_CODE_NOK if failed.
*/
gpNvm_Result gpNvm_GetAttribute ( gpNvm_AttrId attrId,
                                  UInt8 *pLength,
                                  UInt8 *pValue );

/*! \fn gpNvm_Result gpNvm_SetAttribute(gpNvm_AttrId attrId, UInt8 pLength, UInt8* pValue);
    \brief Write \a pValue data linked to targeted \a attrId with \a pLength parameter.
    \param attrId id targeted to get data (input).
    \param pLength number of bytes to write (input).
    \param pValue bytes array to write (input).
    \return success code, will take STORAGE_NVM_RETURN_CODE_OK value if success, STORAGE_NVM_RETURN_CODE_NOK if failed.
*/
gpNvm_Result gpNvm_SetAttribute ( gpNvm_AttrId attrId,
                                  UInt8 length,
                                  UInt8 *pValue );

#endif //CPROJECT_STORAGE_H
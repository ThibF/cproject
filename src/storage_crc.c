#include "storage_crc.h"

UInt8 Crc8 ( const void *vptr, int len )
{
    const UInt8 *data = vptr;
    unsigned crc = 0;
    int i, j;
    for ( j = len; j; j--, data++ )
    {
        crc ^= ( *data << 8 );
        for ( i = 8; i; i-- )
        {
            if ( crc & 0x8000 )
            {
                crc ^= ( 0x1070 << 3 );
            }
            crc <<= 1;
        }
    }
    return (UInt8) ( crc >> 8 );
}

UInt8 get_crc_stored ( UInt8 attrId, UInt8 *crc )
{
    char file_name[MAX_FILENAME_SIZE];
    sprintf ( file_name, "%d-crc", attrId );
    UInt8 length;
    return read_file ( file_name, &length, crc );
}

UInt8 set_crc_stored ( UInt8 attrId, UInt8 crc8 )
{
    char file_name[MAX_FILENAME_SIZE];
    sprintf ( file_name, "%d-crc", attrId );
    return write_file ( file_name, 1, &crc8 );
}

UInt8 get_crc ( UInt8 length, UInt8 *data )
{
    return Crc8 ( data, length );
}

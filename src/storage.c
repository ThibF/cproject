#include "storage.h"

gpNvm_Result gpNvm_GetAttribute ( gpNvm_AttrId attrId, UInt8 *pLength, UInt8 *pValue )
{
    char file_name[MAX_FILENAME_SIZE];
    UInt8 ret_code;
    UInt8 crc_stored;
    sprintf ( file_name, "%d", attrId );
    ret_code = read_file ( file_name, pLength, pValue );
    if ( ret_code == STORAGE_NVM_RETURN_CODE_NOK)
    {
        return ret_code;
    }

    ret_code = get_crc_stored ( attrId, &crc_stored );
    if ( ret_code == STORAGE_NVM_RETURN_CODE_NOK)
    {
        return ret_code;
    }
    if ( crc_stored != get_crc ( *pLength, pValue ))
    {
        return STORAGE_NVM_RETURN_CODE_NOK;
    }
    return STORAGE_NVM_RETURN_CODE_OK;

}

gpNvm_Result gpNvm_SetAttribute ( gpNvm_AttrId attrId, UInt8 pLength, UInt8 *pValue )
{
    char file_name[MAX_FILENAME_SIZE];
    UInt8 crc8 = 0;
    UInt8 ret_code;

    sprintf ( file_name, "%d", attrId );
    ret_code = write_file ( file_name, pLength, pValue );
    if ( ret_code == STORAGE_NVM_RETURN_CODE_NOK)
    {
        return ret_code;
    }
    crc8 = get_crc ( pLength, pValue );

    ret_code = set_crc_stored ( attrId, crc8 );
    if ( ret_code == STORAGE_NVM_RETURN_CODE_NOK)
    {
        return ret_code;
    }

    return STORAGE_NVM_RETURN_CODE_OK;
}

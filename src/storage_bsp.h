#ifndef CPROJECT_STORAGE_BSP_H
#define CPROJECT_STORAGE_BSP_H

#include "stdio.h"
#include "defs.h"
UInt8 write_file ( const char *filename, UInt8 pLength, UInt8 *pValue );
UInt8 read_file ( const char *filename, UInt8 *pLength, UInt8 *pValue );

#endif // CPROJECT_STORAGE_BSP_H
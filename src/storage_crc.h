#ifndef CPROJECT_STORAGE_CRC_H
#define CPROJECT_STORAGE_CRC_H

#include "defs.h"
#include "storage_bsp.h"

UInt8 get_crc_stored ( UInt8 attrId, UInt8 *crc );
UInt8 get_crc ( UInt8 length, UInt8 *data );
UInt8 set_crc_stored ( UInt8 attrId, UInt8 crc8 );

#endif //CPROJECT_STORAGE_CRC_H

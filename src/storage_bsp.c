#include "storage_bsp.h"

extern int errno;

UInt8 write_file ( const char *filename, UInt8 pLength, UInt8 *pValue )
{
    FILE *file_pointer = NULL;
    file_pointer = fopen ( filename, "wb+" );
    if ( file_pointer == NULL)
    {
        return STORAGE_NVM_RETURN_CODE_NOK;
    }
    else
    {
        fwrite ( pValue, 1, pLength, file_pointer );
    }
    fclose ( file_pointer );
    return STORAGE_NVM_RETURN_CODE_OK;
}

UInt8 read_file ( const char *filename, UInt8 *pLength, UInt8 *pValue )
{
    FILE *file_pointer = NULL;
    long file_length = 0;
    file_pointer = fopen ( filename, "rb" );
    if ( file_pointer == NULL)
    {
        *pLength = 0;
        return STORAGE_NVM_RETURN_CODE_NOK;
    }
    fseek ( file_pointer, 0L, SEEK_END );
    file_length = ftell ( file_pointer );
    rewind ( file_pointer );

    if ( file_length > 255 ) // not supported, we read only the first 255 bytes
    {
        *pLength = 255;
    }
    else
    {
        *pLength = (UInt8) file_length;
    }

    fread ( pValue, 1, (size_t) pLength, file_pointer );
    fclose ( file_pointer );
    return STORAGE_NVM_RETURN_CODE_OK;
}

#ifndef CPROJECT_DEFS_H
#define CPROJECT_DEFS_H

typedef unsigned char UInt8;
typedef UInt8 gpNvm_AttrId;
typedef UInt8 gpNvm_Result;

#define MAX_FILENAME_SIZE ((UInt8)30)
#define STORAGE_NVM_RETURN_CODE_NOK ((UInt8)0)
#define STORAGE_NVM_RETURN_CODE_OK ((UInt8)1)

#endif //CPROJECT_DEFS_H

#include "unity.h"
#include "storage_bsp.h"

void setUp ( void )
{
}

void tearDown ( void )
{
    remove ( "25" );
}

//Will write on file system, no way to mock stdio in this framework.
void test_storage_should_return_correctly ( void )
{
    /* Ensure known test state */
    UInt8 data_expected[255] = { 5, 10, 15, 20 };
    UInt8 data[255] = { 5, 10, 15, 20 };
    UInt8 length_expected = 255;
    UInt8 length = 255;
    /* Setup expected call chain */
    //these are defined into assembly instructions.
    /* Call function under test */
    UInt8 ret = 0;
    ret = write_file ( "25", length, data );
    read_file ( "25", &length, data );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_OK, ret );
    TEST_ASSERT_EQUAL ( length_expected, length );
    TEST_ASSERT_EQUAL_INT8_ARRAY ( data, data_expected, length_expected );
}

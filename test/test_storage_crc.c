#include "unity.h"
#include "storage_crc.h"
#include "defs.h"
#include "string.h"
#include "mock_storage_bsp.h"

void setUp ( void )
{
}

void tearDown ( void )
{
}

UInt8 read_real_payload_attrid255 ( const char *filename, UInt8 *length, UInt8 *data, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL_STRING ( "255-crc", filename );
    *length = 1;
    memset ( data, 27, 1 );
    return STORAGE_NVM_RETURN_CODE_OK;
}

UInt8 read_real_payload_attrid255_failed ( const char *filename, UInt8 *length, UInt8 *data, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL_STRING ( "255-crc", filename );
    *length = 1;
    memset ( data, 27, 1 );
    return STORAGE_NVM_RETURN_CODE_NOK;
}

void test_storage_crc_get_crc_stored_should_return_correctly ( void )
{
    /* Ensure known test state */
    UInt8 crc = 0;
    /* Setup expected call chain */
    read_file_StubWithCallback ( &read_real_payload_attrid255 );
    /* Call function under test */
    uint8_t ret = get_crc_stored ( 255, &crc );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_OK, ret );
    TEST_ASSERT_EQUAL ( 27, crc );
}

void test_storage_crc_get_crc_stored_should_return_incorrectly_if_read_failed ( void )
{
    /* Ensure known test state */
    UInt8 crc = 0;
    /* Setup expected call chain */
    read_file_StubWithCallback ( &read_real_payload_attrid255_failed );
    /* Call function under test */
    uint8_t ret = get_crc_stored ( 255, &crc );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_NOK, ret );
}

void test_storage_crc_get_crc_should_correct_crc8 ( void )
{
    UInt8 data[1] = { 0x1 };
    UInt8 length = 1;
    UInt8 crc_returned;
    UInt8 crc_expected = 0x07;
    /* Call function under test */
    crc_returned = get_crc ( length, data );
    TEST_ASSERT_EQUAL ( crc_expected, crc_returned );
}

void test_storage_crc_get_crc_should_correct_crc8_2 ( void )
{
    UInt8 data[8] = { 0xBE, 0xEF, 0, 0, 0, 0, 0, 0 };
    UInt8 length = 2;
    UInt8 crc_returned;
    UInt8 crc_expected = 0x1A;
    /* Call function under test */
    crc_returned = get_crc ( length, data );
    TEST_ASSERT_EQUAL ( crc_expected, crc_returned );
}
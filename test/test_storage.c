#include "unity.h"
#include "storage.h"
#include "defs.h"
#include "string.h"
#include "mock_storage_crc.h"
#include "mock_storage_bsp.h"

void setUp ( void )
{
}

void tearDown ( void )
{
}

UInt8 write_OK ( UInt8 length, UInt8 *data, int cmock_num_calls )
{
    return STORAGE_NVM_RETURN_CODE_OK;
}

UInt8 crc_returned45_attrid36 ( UInt8 attrId, UInt8 *crc, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL ( 36, attrId );
    *crc = 45;
    return STORAGE_NVM_RETURN_CODE_OK;
}

UInt8 crc_returned45_attrid36_return_code_nok ( UInt8 attrId, UInt8 *crc, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL ( 36, attrId );
    *crc = 45;
    return STORAGE_NVM_RETURN_CODE_OK;
}

UInt8 read_empty_attrid36 ( const char *filename, UInt8 *length, UInt8 *data, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL_STRING ( filename, "36" );
    *length = 1;
    *data = 0;
    return STORAGE_NVM_RETURN_CODE_OK;
}

UInt8 get_crc_empty_payload ( UInt8 length, UInt8 *data, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL ( 1, length );
    TEST_ASSERT_EQUAL ( 0, data[ 0 ] );
    return 45;
}

void test_storage_GetAttribute_should_return_correctly_if_everything_is_correct ( void )
{
    /* Ensure known test state */
    UInt8 data[255];
    UInt8 length = 0;
    /* Setup expected call chain */
    read_file_StubWithCallback ( &read_empty_attrid36 );
    get_crc_stored_StubWithCallback ( &crc_returned45_attrid36 );
    get_crc_StubWithCallback ( &get_crc_empty_payload );
    /* Call function under test */
    uint8_t ret = gpNvm_GetAttribute ( 36, &length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_OK, ret );
}

UInt8 read_real_payload_attrid255 ( const char *filename, UInt8 *length, UInt8 *data, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL_STRING ( filename, "255" );
    *length = 255;
    memset ( data, 27, 256 );
    return STORAGE_NVM_RETURN_CODE_OK;
}

UInt8 get_crc_real_payload_crc2 ( UInt8 length, UInt8 *data, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL ( 255, length );
    TEST_ASSERT_EQUAL ( 27, data[ 0 ] );
    TEST_ASSERT_EQUAL ( 27, data[ 255 ] );
    return 2;
}

UInt8 crc_returned2_attrid255 ( UInt8 attrId, UInt8 *crc, int cmock_num_calls )
{
    TEST_ASSERT_EQUAL ( 255, attrId );
    *crc = 2;
    return STORAGE_NVM_RETURN_CODE_OK;
}

void test_storage_GetAttribute_should_return_correctly_if_everything_is_correct_with_big_payload ( void )
{
    /* Ensure known test state */
    UInt8 data[256];
    UInt8 length = 0;
    UInt8 data_expected[256];
    UInt8 length_expected = 255;
    memset ( data_expected, 27, 256 );

    /* Setup expected call chain */
    read_file_StubWithCallback ( &read_real_payload_attrid255 );
    get_crc_stored_StubWithCallback ( &crc_returned2_attrid255 );
    get_crc_StubWithCallback ( &get_crc_real_payload_crc2 );
    /* Call function under test */
    uint8_t ret = gpNvm_GetAttribute ( 255, &length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_OK, ret );
    TEST_ASSERT_EQUAL ( 255, length );
    TEST_ASSERT_EQUAL_INT8_ARRAY ( data, data_expected, length_expected );

}

void test_storage_GetAttribute_should_return_error_if_crc_different ( void )
{
    /* Ensure known test state */
    UInt8 data[255];
    UInt8 length = 0;
    /* Setup expected call chain */
    read_file_CMockIgnoreAndReturn ( __LINE__, STORAGE_NVM_RETURN_CODE_OK );
    get_crc_stored_StubWithCallback ( &crc_returned45_attrid36 );
    get_crc_CMockIgnoreAndReturn ( __LINE__, 46 );
    /* Call function under test */
    uint8_t ret = gpNvm_GetAttribute ( 36, &length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_NOK, ret );
}

void test_storage_GetAttribute_should_return_error_if_read_fail ( void )
{
    /* Ensure known test state */
    UInt8 data[255];
    UInt8 length = 0;
    /* Setup expected call chain */
    read_file_CMockIgnoreAndReturn ( __LINE__, STORAGE_NVM_RETURN_CODE_NOK );
    /* Call function under test */
    uint8_t ret = gpNvm_GetAttribute ( 36, &length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_NOK, ret );
}

void test_storage_GetAttribute_should_return_error_if_crc_read_failed ( void )
{
    /* Ensure known test state */
    UInt8 data[255];
    UInt8 length = 0;
    /* Setup expected call chain */
    read_file_CMockIgnoreAndReturn ( __LINE__, STORAGE_NVM_RETURN_CODE_OK );
    get_crc_CMockIgnoreAndReturn ( __LINE__, 46 );
    get_crc_stored_StubWithCallback ( &crc_returned45_attrid36_return_code_nok );
    /* Call function under test */
    uint8_t ret = gpNvm_GetAttribute ( 36, &length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_NOK, ret );
}

void test_storage_SetAttribute_should_return_success_if_nothing_wrong ( void )
{
    /* Ensure known test state */
    UInt8 data[255];
    UInt8 length = 20;
    /* Setup expected call chain */
    write_file_CMockExpectAndReturn ( __LINE__, "36", length, data, STORAGE_NVM_RETURN_CODE_OK );
    get_crc_CMockExpectAndReturn ( __LINE__, length, data, 45 );
    set_crc_stored_CMockExpectAndReturn ( __LINE__, 36, 45, STORAGE_NVM_RETURN_CODE_OK );
    /* Call function under test */
    uint8_t ret = gpNvm_SetAttribute ( 36, length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_OK, ret );
}

void test_storage_SetAttribute_should_return_error_if_write_failed ( void )
{
    /* Ensure known test state */
    UInt8 data[255];
    UInt8 length = 20;
    /* Setup expected call chain */
    write_file_CMockExpectAndReturn ( __LINE__, "36", length, data, STORAGE_NVM_RETURN_CODE_NOK );
    /* Call function under test */
    uint8_t ret = gpNvm_SetAttribute ( 36, length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_NOK, ret );
}

void test_storage_SetAttribute_should_return_error_if_crc_write_failed ( void )
{
    /* Ensure known test state */
    UInt8 data[255];
    UInt8 length = 20;
    /* Setup expected call chain */
    write_file_CMockExpectAndReturn ( __LINE__, "36", length, data, STORAGE_NVM_RETURN_CODE_OK );
    get_crc_CMockExpectAndReturn ( __LINE__, length, data, 45 );
    set_crc_stored_CMockExpectAndReturn ( __LINE__, 36, 45, STORAGE_NVM_RETURN_CODE_NOK );
    /* Call function under test */
    uint8_t ret = gpNvm_SetAttribute ( 36, length, &data[ 0 ] );

    /* Verify test results */
    TEST_ASSERT_EQUAL ( STORAGE_NVM_RETURN_CODE_NOK, ret );
}